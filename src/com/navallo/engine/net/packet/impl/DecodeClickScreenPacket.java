package com.navallo.engine.net.packet.impl;

import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.world.entity.player.Player;

/**
 * Sent when the player clicks anywhere on the game screen.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 241 })
public class DecodeClickScreenPacket extends PacketDecoder {

    @Override
    public void decode(Player player, ProtocolBuffer buf) {

    }
}
