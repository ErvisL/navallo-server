package com.navallo;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.navallo.engine.GameEngine;
import com.navallo.engine.net.HostGateway;
import com.navallo.engine.net.ServerEngine;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.task.TaskManager;
import com.navallo.util.Stopwatch;
import com.navallo.world.entity.combat.effect.CombatPoisonEffect.CombatPoisonData;
import com.navallo.world.entity.npc.Npc;
import com.navallo.world.entity.npc.NpcAggression;
import com.navallo.world.entity.npc.NpcDefinition;
import com.navallo.world.entity.npc.NpcDropTable;
import com.navallo.world.entity.player.content.RestoreStatTask;
import com.navallo.world.entity.player.content.SkillRequirements;
import com.navallo.world.entity.player.content.WeaponAnimations;
import com.navallo.world.entity.player.content.WeaponInterfaces;
import com.navallo.world.entity.player.minigame.Minigames;
import com.navallo.world.entity.player.skill.Skills;
import com.navallo.world.item.ItemDefinition;
import com.navallo.world.item.ground.GroundItemManager;
import com.navallo.world.object.WorldObjectManager;
import com.navallo.world.shop.Shop;

/**
 * The main class of this server.
 * 
 * @author lare96
 */
public final class Main {

    /** The logger for printing information. */
    private static Logger logger = Logger.getLogger(Main.class.getSimpleName());

    /** The name of this server. */
    public static final String NAME = "Navallo";


    /**
     * The default constructor, will throw an
     * {@link UnsupportedOperationException} if instantiated.
     */
    private Main() {
        throw new UnsupportedOperationException(
            "This class cannot be instantiated!");
    }

    /**
     * The main method of this server.
     * 
     * @param args
     *            the array of runtime arguments.
     */
    public static void main(String[] args) {
        try {

            // The stopwatch for timing how long all this takes.
            Stopwatch timer = new Stopwatch().reset();

            // Load all utilities.
            NpcDropTable.parseDrops().load();
            ItemDefinition.parseItems().load();
            WorldObjectManager.parseObjects().load();
            NpcDefinition.parseNpcs().load();
            Shop.parseShops().load();
            GroundItemManager.parseItems().load();
            Npc.parseNpcs().load();
            SkillRequirements.parseRequirements().load();
            WeaponAnimations.parseAnimations().load();
            WeaponInterfaces.parseInterfaces().load();
            HostGateway.loadBannedHosts();
            Skills.loadSkills();
            PacketDecoder.loadDecoders();
            Minigames.loadMinigames();
            CombatPoisonData.loadPoisonData();
            NpcAggression.loadPolicies();
            logger.info("Sucessfully loaded all utilities!");

            // Initialize and start the reactor.
            ServerEngine.init();
            logger.info("The reactor is now running!");

            // Initialize and start the engine.
            GameEngine.init();
            logger.info("The engine is now running!");

            // Asteria is now online!
            logger.info(NAME + " is now online! [took " + timer.elapsed() + "ms]");

            // Start miscellaneous tasks.
            TaskManager.submit(new RestoreStatTask());
            TaskManager.submit(new GroundItemManager());
        } catch (Exception e) {

            // An error occurred, print it.
            logger.log(Level.SEVERE,
                "An error occured while starting " + Main.NAME + "!", e);
        }
    }
}
