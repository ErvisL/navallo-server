package com.navallo.engine.net.packet.impl;

import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.world.entity.player.Player;

/**
 * Sent when the player clicks certain options on an interface.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 130 })
public class DecodeInterfaceClickPacket extends PacketDecoder {

    @Override
    public void decode(Player player, ProtocolBuffer buf) {
        if (player.getTradeSession().inTrade()) {
            player.getTradeSession().reset(true);
        }

        player.getPacketBuilder().sendCloseWindows();
    }
}
