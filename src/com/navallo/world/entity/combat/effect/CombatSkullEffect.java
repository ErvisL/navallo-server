package com.navallo.world.entity.combat.effect;

import com.navallo.engine.task.Task;
import com.navallo.world.entity.UpdateFlags.Flag;
import com.navallo.world.entity.player.Player;

/**
 * A {@link Task} implementation that will remove the white skull from above
 * player's head.
 * 
 * @author lare96
 */
public class CombatSkullEffect extends Task {

    /** The player attached to this task. */
    private Player player;

    /**
     * Create a new {@link CombatSkullEffect}.
     * 
     * @param player
     *            the player attached to this task.
     */
    public CombatSkullEffect(Player player) {
        super(50, false);
        super.bind(player);
        this.player = player;
    }

    @Override
    public void execute() {

        // Timer is at or below 0 so we can remove the skull.
        if (player.getSkullTimer() <= 0) {
            player.setSkullIcon(-1);
            player.getFlags().flag(Flag.APPEARANCE);
            this.cancel();
            return;
        }

        // Otherwise we just decrement the timer.
        player.decrementSkullTimer();
    }
}
