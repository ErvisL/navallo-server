package com.navallo.engine.net.packet.impl;

import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.ProtocolBuffer.ByteOrder;
import com.navallo.engine.net.ProtocolBuffer.ValueType;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.util.Utility;
import com.navallo.world.entity.player.Player;
import com.navallo.world.entity.player.content.FoodConsumable;
import com.navallo.world.entity.player.content.PotionConsumable;
import com.navallo.world.entity.player.skill.Skills;
import com.navallo.world.entity.player.skill.impl.Fishing;
import com.navallo.world.item.Item;
import com.navallo.world.item.ItemDefinition;

/**
 * Sent when the player uses the first click item option.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 122 })
public class DecodeClickItemPacket extends PacketDecoder {

    @Override
    public void decode(Player player, ProtocolBuffer buf) {
        int container = buf.readShort(true, ValueType.A, ByteOrder.LITTLE);
        int slot = buf.readShort(false, ValueType.A);
        int id = buf.readShort(false, ByteOrder.LITTLE);

        if (slot < 0 || container < 0 || id < 0 || id > ItemDefinition.getDefinitions().length) {
            return;
        }

        Skills.fireSkillEvents(player);
        player.getCombatBuilder().cooldown(true);

        if (container == 3214) {
            Item item = player.getInventory().get(slot);

            if (item == null || item.getId() != id) {
                return;
            }

            if (FoodConsumable.consume(player, item, slot)) {
                return;
            }

            if (PotionConsumable.consume(player, item, slot)) {
                return;
            }

            switch (item.getId()) {

            case 405: // Casket obtained from fishing.
                if (player.getInventory().add(
                    Utility.randomElement(Fishing.CASKET_ITEMS).clone())) {
                    player.getInventory().remove(item, slot);
                    player.getPacketBuilder().sendMessage(
                        "You open the casket and recieve an item!");
                }
                break;
            }
        }
    }
}