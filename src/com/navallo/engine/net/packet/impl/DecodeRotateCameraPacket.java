package com.navallo.engine.net.packet.impl;

import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.world.entity.player.Player;

/**
 * Sent when the player uses the arrow keys to rotate the camera.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 86 })
public class DecodeRotateCameraPacket extends PacketDecoder {

    @Override
    public void decode(Player player, ProtocolBuffer buf) {

    }
}
