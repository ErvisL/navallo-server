package com.navallo.engine.net.packet.impl;

import com.navallo.Main;
import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.world.Constants;
import com.navallo.world.entity.player.Player;
import com.navallo.world.item.ground.GroundItemManager;
import com.navallo.world.object.WorldObjectManager;

/**
 * Sent when the player loads a new map region.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 121 })
public class DecodeUpdateRegionPacket extends PacketDecoder {

    @Override
    public void decode(Player player, ProtocolBuffer buf) {

        // To prevent abuse of this packet, imagine someone attempting to inject
        // this 500 or so times.
        if (player.isUpdateRegion()) {
            WorldObjectManager.load(player);
            GroundItemManager.load(player);
            player.displayInterfaces();
            player.getTolerance().reset();
            player.setUpdateRegion(false);

            if (Constants.DEBUG)
                player.getPacketBuilder().sendMessage(
                    "DEBUG[region= " + player.getPosition().getRegion() + "]");
        }
    }
}
