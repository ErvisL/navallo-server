package com.navallo.world;

import com.navallo.Main;
import com.navallo.world.item.Item;
import com.navallo.world.map.Position;

/**
 * Created by Ervis on 08/11/2014.
 */
public class Constants {

    /**
     * Change this when there is a client update and we require the users to download the latest client
     */
    public static int VERSION = 317;

    /**
     * The welcome message.
     */
    public static final String FIRST_MESSAGE = "Welcome to " + Main.NAME + "!";

    /**
     * Secondary message sent upon login
     */
    public static final String SECONDARY_LOGIN_MESSAGE = "You're currently playing the beta phase, you can use the following commands bellow";

    /**
     * Third message sent upon login
     */
    public static final String THIRD_LOGIN_MESSAGE = "You can set your levels by using ::setlevel or spawn items using ::item or ::pickup";

    /**
     * The items received when the player logs in for the first time.
     */
    public static final Item[] STARTER_PACKAGE = {new Item(995, 10000)};

    /**
     * The starting position.
     */
    public static final Position STARTING_POSITION = new Position(3093, 3244);

    /**
     * If debugging messages should be printed.
     */
    public static final boolean DEBUG = true;
}
