package com.navallo.engine.net.packet.impl;

import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.ProtocolBuffer.ValueType;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.world.entity.player.Player;
import com.navallo.world.entity.player.skill.Skills;
import com.navallo.world.item.Item;
import com.navallo.world.item.ItemDefinition;
import com.navallo.world.item.ground.GroundItem;
import com.navallo.world.item.ground.GroundItemManager;
import com.navallo.world.map.Position;

/**
 * Sent when the player drops an item.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 87 })
public class DecodeDropItemPacket extends PacketDecoder {

    // TODO: Proper validation.

    @Override
    public void decode(Player player, ProtocolBuffer buf) {
        int item = buf.readShort(false, ValueType.A);
        buf.readByte(false);
        buf.readByte(false);
        int slot = buf.readShort(false, ValueType.A);

        if (slot < 0 || item < 0) {
            return;
        }

        Skills.fireSkillEvents(player);

        if (player.getInventory().contains(item)) {
            int amount = ItemDefinition.getDefinitions()[item].isStackable() ? amount = player.getInventory().totalAmount(
                item)
                : 1;

            player.getInventory().remove(new Item(item, amount), slot);
            Position itemLocation = new Position(player.getPosition().getX(),
                player.getPosition().getY(), player.getPosition().getZ());
            GroundItemManager.register(new GroundItem(new Item(item, amount),
                itemLocation, player));
        }
    }
}
