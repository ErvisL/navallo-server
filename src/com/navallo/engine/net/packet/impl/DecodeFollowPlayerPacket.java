package com.navallo.engine.net.packet.impl;

import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.ProtocolBuffer.ByteOrder;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.world.World;
import com.navallo.world.entity.player.Player;
import com.navallo.world.entity.player.skill.Skills;

/**
 * Sent when the player tries to follow another player.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 39 })
public class DecodeFollowPlayerPacket extends PacketDecoder {

    @Override
    public void decode(Player player, ProtocolBuffer buf) {
        int followId = buf.readShort(false, ByteOrder.LITTLE);

        if (followId < 0) {
            return;
        }
        Player follow = World.getPlayers().get(followId);

        if (follow == null || !follow.getPosition().isViewableFrom(
            player.getPosition()) || follow.equals(player)) {
            return;
        }

        Skills.fireSkillEvents(player);
        player.getMovementQueue().follow(follow);
    }
}
