package com.navallo.engine.net.packet.impl;

import com.navallo.engine.net.ProtocolBuffer;
import com.navallo.engine.net.packet.PacketDecoder;
import com.navallo.engine.net.packet.PacketOpcodeHeader;
import com.navallo.world.entity.player.Player;

/**
 * An empty decoder executed when unused packets are sent.
 * 
 * @author lare96
 */
@PacketOpcodeHeader({ 0 })
public class DecodeDefaultPacket extends PacketDecoder {

    @Override
    public void decode(Player player, ProtocolBuffer buf) {

    }
}
